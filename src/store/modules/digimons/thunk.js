import axios from "axios";
import { toast } from "react-toastify";
import { addDigimon } from "./actions";

const addDigimonsThunk = (digimon, setError) => {
  return (dispatch, getState) => {
    const digimonsReducer = getState();
    console.log(digimonsReducer);
    axios
      .get(`https://digimon-api.vercel.app/api/digimon/name/${digimon}`)
      .then((response) => {
        if (digimonsReducer.digimon.length > 0) {
          const dig = digimonsReducer.digimon.flat();

          let hasDiferent = false;
          console.log(dig);
          dig.map(
            (digimon, index) =>
              response.data[0].name === digimon.name && (hasDiferent = true)
          );

          if (hasDiferent === false) {
            dispatch(addDigimon([...digimonsReducer.digimon, response.data]));
          }
        } else {
          console.log("oi");
          dispatch(addDigimon([...digimonsReducer.digimon, response.data]));
        }
      })
      .catch(() => toast.error("Digimon não encontrado!"));
  };
};
export default addDigimonsThunk;
