import { useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import addDigimonsThunk from "../../store/modules/digimons/thunk";

const Search = () => {
  const [input, setInput] = useState("");
  const [error, setError] = useState(false);

  const dispatch = useDispatch();

  const handleSearch = () => {
    setError(false);
    dispatch(addDigimonsThunk(input, setError));
  };
  console.log(error);
  return (
    <>
      <div>{error === true ? toast.error("Oi") : false}</div>
      <div>
        <h2>Procure pelo seu Digimon!!!</h2>

        <div>
          <input
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="Procure seu Digimon"
          />
          <button onClick={() => handleSearch()}>Pesquisar</button>
        </div>
      </div>
    </>
  );
};

export default Search;
