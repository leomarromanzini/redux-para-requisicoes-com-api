import { useSelector } from "react-redux";
import Digimon from "../Digimon";

const DigimonList = () => {
  const { digimon } = useSelector((state) => state);

  return (
    <div className="container">
      {digimon.map((digimon, index) => (
        <Digimon key={index}>{digimon[0]}</Digimon>
      ))}{" "}
    </div>
  );
};

export default DigimonList;
