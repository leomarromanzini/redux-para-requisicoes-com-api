import "./styles.css";

const Digimon = ({ children }) => {
  return (
    <div className="cardContainer">
      <p>{children.name}</p>
      <img src={children.img} alt="diigmon"></img>
      <p>{children.level}</p>
    </div>
  );
};

export default Digimon;
